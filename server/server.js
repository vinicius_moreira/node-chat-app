const express = require('express');
const http = require('http')
const path = require('path');
const port = process.env.PORT || 3000;
const socketIO = require('socket.io');

const {generateMessage, generateLocationMessage} = require('./utils/message');
const {isRealString} = require('./utils/validation');
const {Users} = require('./utils/users');

const publicPath = path.join(__dirname, '../public');
// é melhor que utilizar somente __dirname + '../public'. Porque o caminho final ficaria relativo: server/../public
// com a biblioteca 'path', se transforma em caminho absoluto: /node-chat-app/public

var app = express();
var server = http.createServer(app);
var io = socketIO(server);
var users = new Users();

app.use(express.static(publicPath));

io.on('connection', (socket) => {
  console.log('New user connected');

  socket.on('join', (params, callback) => {
    if (!isRealString(params.name) || !isRealString(params.room)) {
      return callback('"name" and "room" are required');
    }

    socket.join(params.room);
    users.removeUser(socket.id);
    users.addUser(socket.id, params.name, params.room);

    io.to(params.room).emit('updateUserList', users.getUserList(params.room));
    socket.emit('newMessage', generateMessage('admin', 'Welcome to the chat app')); // it emits only for the new connection
    socket.broadcast.to(params.room).emit('newMessage', generateMessage('admin', `${params.name} has joined`)); // broadcast to all (but the new connection)

    callback();
  });

  socket.on('createMessage', (message, callback) => {

    var user = users.getUser(socket.id);

    if (user && isRealString(message.text)){
      io.to(user.room).emit('newMessage', generateMessage(user.name, message.text)); // broadcast to all clients (including the sender)
      //socket.broadcast.emit('newMessage', generateMessage(message.from, message.text)); // broadcast to all (but the sender)
      //socket.broadcast.to('room xpto').emit('newMessage', generateMessage(message.from, message.text)); // broadcast to all in a specific room (but the sender)
    }

    callback(); // acknowledges to the client that the message has arrived
  });

  socket.on('createLocationMessage', (coords) => {

    var user = users.getUser(socket.id);

    if (user) {
      io.to(user.room).emit('newLocationMessage', generateLocationMessage(user.name, coords.latitude, coords.longitude));
    }

  });

  socket.on('disconnect', () => {
    var user = users.removeUser(socket.id);

    if (user){
      io.to(user.room).emit('updateUserList', users.getUserList(user.room));
      io.to(user.room).emit('newMessage', generateMessage('admin', `${user.name} has left.`));
    }
  });
}); // ouve quando uma nova conexão ocorre


server.listen(port, () => console.log(`Server running at ${port}`)); // chama http.createServer() por baixo dos panos
