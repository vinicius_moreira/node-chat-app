var expect = require('expect');

var {generateMessage, generateLocationMessage} = require('./message');

describe('generateMessage', () => {

  it('should generate a correct message object', () => {
      var from = 'vinicius';
      var text = 'some text';

      var res = generateMessage(from, text);
      expect(res).toInclude({ from, text });
      expect(res.createdAt).toBeA('number');
  });

});

describe('generateLocationMessage', () => {

  it('should generate the correct location object', () => {
    var from = 'admin';
    var latitude = 1;
    var longitude = 2;
    var url =  `https://www.google.com/maps?q=${latitude},${longitude}`;

    var res = generateLocationMessage(from, latitude, longitude);
    expect(res.createdAt).toBeA('number');
    expect(res).toInclude({ from, url });
  });

});
