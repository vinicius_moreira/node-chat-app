const expect = require('expect');
const {Users} = require('./users');

describe('Users', () => {

  var users;

  beforeEach(() => {
    users = new Users();
    users.users = [{
        id : '1',
        name: 'John',
        room : 'Room 1'
      }, {
        id : '2',
        name: 'Ana',
        room : 'Room 2'
      }, {
        id : '3',
        name: 'Mike',
        room : 'Room 1'
    }];
  });

  it('should add new user', () => {
    var users = new  Users();
    var user = {
      id: '123',
      name: 'John',
      room: 'xpto'
    };

    var resUser = users.addUser(user.id, user.name, user.room);
    expect(users.users).toEqual([user]);
  });

  it('should return names for "Room 1"', () => {
    var res = users.getUserList('Room 1');

    expect(res).toEqual(['John', 'Mike']);
  });

  it('should return names for "Room 2"', () => {
    var res = users.getUserList('Room 2');

    expect(res).toEqual(['Ana']);
  });

  it('should remove a user', () => {
      var user = users.removeUser('2');

      expect(user.id).toBe('2');
      expect(users.users.length).toBe(2);
  });

  it('should not remove a user with a non-existing id', () => {
    var user = users.removeUser('4');

    expect(user).toNotExist();
    expect(users.users.length).toBe(3);
  });

  it('should find a user', () => {
    var user = users.getUser('2');

    expect(user.id).toBe('2');
  });

  it('should not find a user with a non-existing id', () => {
    var user = users.getUser('4');

    expect(user).toNotExist();
  });
});
